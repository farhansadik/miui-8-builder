#!/bin/bash
#!/system/bin/sh
#!/system/bin/bash

	# MIUI 8 Custom OS Build & Compiler

set import bash from busybox
set import zip from busybox

DEF_LOC=~/builder_miui
OUT=/tmp/report.log
FILE_1=MT6580_MIUI_8_For_iris_821_www.romclaims.com.zip
FILE_2=build.zip
FILE_3=file1.zip # 320 Lcd_Density Patch.zip
FILE_4=file2.zip # MIUI_8_FM_ _Bluetooth_Fix_www.romclaims.com.zip
FILE_5=file3.zip # Vendor_Blobs_Lava_iris_820_ _821_ www.romclaims.com .zip

{ # core
	clear; echo "Script Version 0.1 COIL" && echo; cd $DEF_LOC;
	printf "Process 1 : Unzip Package\n";
	unzip $FILE_1 >> $OUT && rm -r $FILE_1;
	printf "Process 2 : Deleting Package\n";
	rm -r META-INF; cd system; rm -r build.prop; cd ..;	
	printf "Process 3 : Unpacking Build Package\n";
	unzip $FILE_2 >> $OUT; rm -r $FILE_2;	
	printf "Process 4 : Building MIUI Installer\n";
	zip -r miui_8_installer.zip boot.img  busybox  data  file_contexts	logo.bin  META-INF  system >> $OUT
	printf "Process 5: Deleting Cache\n";
	rm -r META-INF  system
	printf "Process 6: Deleting Extra Files\n";
	rm -r boot.img  busybox  data  file_contexts logo.bin
	printf "Process 7: Building Updates\n";
	if unzip $FILE_3 >> $OUT; then rm -r $FILE_3; fi
	if unzip $FILE_4 >> $OUT; then rm -r $FILE_4; fi
	if unzip $FILE_5 >> $OUT; then rm -r $FILE_5; fi
	printf "Process 8: Compiling Updates\n";
	zip -r ota_updater_miui_8.zip META-INF system >> $OUT; rm -rfv META-INF system >> $OUT;
	printf "Building Compleated! Flash those file using Custom Recovery\n"; echo 
	printf "\033[0;32mProgram by \033[0;31mFarhan Sadik.\33[m\n"
	printf "\033[0;32mBuilt v0.1 COIL created with Shell Script on Arch Linux.\33[m\n"
	echo && exit 0;
}

# start date 11 Dec 2017
# created by farhan sadik
# square development group
