# MIUI 8 Builder 

MIUI custom rom for Lava Iris 820. Original build is `MT6580_MIUI_8_For_iris_821_www.romclaims.com.zip`. This simple script will fix this custom rom and compile it to a new source. 

### Hardware Requirments : 
 - 1.Android Device ~ Lava Iris 820 1/2Gib 
 - 2.Computer - Linux Distribution (Ubuntu/Arch)

### File Requirements : 
 - 1.320 Lcd_Density Patch.zip
 - 2.MIUI_8_FM_ _Bluetooth_Fix_www.romclaims.com.zip
 - 3.Vendor_Blobs_Lava_iris_820_ _821_ www.romclaims.com .zip

### Before compile : 
please rename those file 
 - 1.`320 Lcd_Density Patch.zip` to `file1.zip`
 - 2.`MIUI_8_FM_ _Bluetooth_Fix_www.romclaims.com.zip` to `file2.zip` 
 - 3.`Vendor_Blobs_Lava_iris_820_ _821_ www.romclaims.com .zip` to file3.zip`

### Compile Process : 
 - 1.Create a folder in /home, Example ~ `/home/builder/`
 - 2.Copy all necessary file to `/home/builder/` and place builder script at that folder.
 - 3.Then enter command - 
    > $ chmod +x build.bash
 - 4.Then enter command - 
    > $ ./build.bash

At the last moment you've got a file named `ota_updater_miui_8.zip`. Then just flash it to your android device using recovery tools. 

***Farhan Sadik***

***Square Development Group***
